from pymongo import MongoClient
import sys
url = 'mongodb://ruchika:111000zmz@ds347367.mlab.com:47367/records'
client = MongoClient(url)
db = client.get_default_database("records")

def main():
 
  
    budgets = db.budgets


    while(1):
        selection = raw_input('\nSelect 1 to insert, 2 to update, 3 to read, 4 to delete\n')

        if selection == '1':
            insert()
        elif selection == '2':
            update()
        elif selection == '3':
            read()
        elif selection == '4':
            delete()
        else:
            print '\n not valid!'



def insert():
    try:
        employeeId = raw_input('Enter Employee Id: ')
        employeeName = raw_input('enter the name: ')
        employeeAge = raw_input('enter the age: ')
        employeeCountry = raw_input('enter country')

        db.budgets.insert_one(
            {
                "Id":employeeId,
                "name": employeeName,
                "Age": employeeAge,
                "Country": employeeCountry   
            }
            )
        print '\nInserted data successfully\n'

    except Exception, e:
        print str(e)

def read():
    try:
        lists = db.budgets.find()
        for a in lists:
            print a
    except Exception, e:
        print str(e)
    
def update():
    try:
        criteria = raw_input('\nEnter id to update\n')
        name = raw_input('\nEnter name to update\n')
        age = raw_input('\nEnter age to update\n')
        country = raw_input('\nEnter country to update\n')

        db.budgets.update_one(
            {"Id": criteria},
            {
                "$set": {
                    "name":name,
                    "Age":age,
                    "Country":country
                }
            }
        )
        print "\nRecords updated successfully\n"  
    
    except Exception, e:
        print str(e)

def delete():
    try:
        
        db.Employees.remove()
        print '\nDeletion successful\n' 
    except Exception, e:
        print str(e)

if __name__ == '__main__':
    main()

