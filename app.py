from flask import Flask, render_template, request, url_for, redirect, jsonify, json
from pymongo import MongoClient
from bson import ObjectId
import os

app = Flask(__name__)

url = 'mongodb://ruchika:111000zmz@ds347367.mlab.com:47367/records'
client = MongoClient(url)
db = client.get_default_database("records")

budgets = db.budgets

@app.route("/", methods=['POST', 'GET'])
def home():
    if request.method == "POST":
        # response = request.get_json()
        category = request.form['category']
        budget = request.form['budget']
        spent = request.form['spent']
        remaining = int(budget)-int(spent)

        budgets.insert_one({
            "category": category,
            "budget": budget,
            "spent": spent,
            "remaining": remaining
        })
    return render_template("home.html")

@app.after_request
def add_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    return response

@app.route("/lists", methods=['GET'])
def lists():
    if request.method == 'GET':
        rows = budgets.find()
        return render_template('list.html', rows=rows)
        
@app.route("/update/<budget_id>", methods =["GET", "POST"])
def update(budget_id):
    if request.method == "GET":
        item = budgets.find_one({"_id": ObjectId(budget_id)})
    return render_template('update.html', item=item)

    if request.method == "POST":
        newid = ObjectId(budget_id)
        category = request.form['categoty']
        budget = request.form['budget']
        spent = request.form['spent']
        remaining = request.form['remaining']
        # import pdb; pdb.set_trace()
        budgets.update_one({"_id":ObjectId(newid)}, {'$set':{"category": category, "budget":budget, "spent": spent, "remaining": remaining }})
        return redirect("/lists")

    elif request.method == "POST":
        item_del = budgets.find_one_and_delete({"_id":ObjectId(budget_id)})
        if item_del:
            return redirect("/")
        else:
            print("Something went crazy. Debug.")
    else:
        return 

@app.route("/remove/<budget_id>", methods= ["GET"])
def remove(budget_id):
    if request.method == "GET":
        budgets.remove({"_id": ObjectId(budget_id)})
        return redirect('/lists')

if __name__ == "__main__":
    app.run(debug=True, port=5000)
